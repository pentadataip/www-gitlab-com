---
layout: handbook-page-toc
title: "Commercial TAM Direction"
---

## Commercial TAM Direction  

For customers who qualify for TAM services ([link](https://about.gitlab.com/handbook/customer-success/tam/services/#commercial)), we will continue to expand our reach and effectiveness per TAM to be more efficient and reach more customers in FY22. We will rely on digital programs ([link](https://about.gitlab.com/handbook/customer-success/tam/digital-journey/)) to enable smaller customers, while we improve service level and efficiency for larger customers. Commercial TAMs will leverage the digital journey to assist their efforts, and create repeatable motions such that TAMs can focus on new challenges instead of repeating work for solved problems.

#### TAM Metrics
Some of the ways we measure our effectiveness, and provide insights into our practice include:
- [Core TAM metrics](https://about.gitlab.com/handbook/customer-success/tam/customer-segments-and-metrics/)
- [Video overview](https://www.youtube.com/watch?v=9b8VviLG3yE&t=2s))  

### 1 Year Plan: What’s Next for Commercial TAMs  
**Iterate 2-3 times to refine “TAM managed” engagement model (all >$50k ARR)**
- Improve efficiency for TAM managed accounts
- Continued high rate of growth and renewals
- Scale to meet increased growth targets in FY22
- Iteration 1 - (FY22Q1) high touch + low touch + named ([issue](https://gitlab.com/gitlab-com/customer-success/commercial-markets-initiatives/-/issues/208))
- Iteration 2 - (FY22Q3) TBD, single project, triage, product usage data insights
- Iteration 3 - (FY22Q4) TBD, Success Plan complexity, customer ability
- For each iteration, define metrics and early warning indicators

**Increase consistency and accuracy of Success Planning for Commercial TAMs ([issue](https://gitlab.com/gitlab-com/customer-success/okrs/-/issues/121))**
- Target 100% success plans for all Priority 1 customers (link)
- Improve definition of “green” success plan
- Coaching and peer review of success plans to ensure quality

**Pilot a pooled model for smaller customers**
- Setup a service desk pool for customers to get help
- Define qualification criteria and TAM engagement model for the pool
- Create a triage workflow to identify and assist stuck customers
Outcome:  Learn new ways to efficiently help smaller customers who would not otherwise be successful

**Run 2 pilots for customer cohorts to learn how customers can better help each other**  
For each:
-  Choose a shared collaboration platform and namespace
-  Research cohort size, engagement rules and scope
-  Assign TAMs to manage, monitor, and report

**Resume enablement webinar series to unlock growth and time to value ([issue](https://gitlab.com/gitlab-com/customer-success/okrs/-/issues/129))**  
Suggested topics:
-  CI/CD for new users and admins
-  Secure - Scan setup and basic troubleshooting, working with the results
-  GitLab approvals, gates, and compliance workflows
-  GitOps, Configure, Package and Release for DevOps teams
