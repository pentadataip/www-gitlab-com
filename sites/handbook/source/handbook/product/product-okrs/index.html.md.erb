---
layout: handbook-page-toc
title: Product Organization OKRs
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

[**Principles**](/handbook/product/product-principles/) - [**Processes**](/handbook/product/product-processes/) - [**Categorization**](/handbook/product/categories/) - [**GitLab the Product**](/handbook/product/gitlab-the-product) - [**Being a PM**](/handbook/product/product-manager-role) - [**Performance Indicators**](/handbook/product/performance-indicators/) - [**Leadership**](/handbook/product/product-leadership/)

## GitLab OKRs

GitLab uses the [OKR (Objective and Key Results) framework](https://www.youtube.com/watch?v=L4N1q4RNi9I) to set and track goals on a quarterly basis. You can read more about OKRs at GitLab OKRs including the overall process as well as current and past OKRs [here](https://about.gitlab.com/company/okrs/). Every department participates in OKRs to collaboratively goal set across the organization. GitLab does quarterly objectives which align with our Fiscal Year. 

## Product OKRs

Currently, Product team OKRs are tracked as Epics [gitlab.com](https://gitlab.com/groups/gitlab-com/-/epics?scope=all&state=opened&search=FY22-Q2+OKR) and the KRs are tracked as Issues in the [Product project](https://gitlab.com/gitlab-com/Product) and on the [Product OKR Board](https://gitlab.com/gitlab-com/Product/-/boards/906048?label_name%5B%5D=OKR). Starting in FY22-Q2, all OKRs are also in [Ally.io](https://app.ally.io/teams/39480/objectives?tab=0&time_period_id=135090) so  we can get familiar with the tool before FY22-Q3. Starting in FY22-Q3, we're aiming to replace use of GitLab epics, issues and boards and fully port all OKRs into Ally.io. Details on that process and instructions on how to participate will be added to this page as they solidify. 

To learn more about the rollout process for Product and Engineering see the linked [Ally OKR Rollout GitLab Project](https://gitlab.com/gitlab-com/ally-okr-rollout).

### Product OKR Process

Each organization at GitLab may follow a slightly personalized process for OKRs. Below is the process for Product. Product works closely with Engineering and collaborates on some Objectives and Key Results. You can reference the Engineering's process [here](https://about.gitlab.com/handbook/engineering/#engineering-okr-process).

The Product OKRs are initiated by the Chief Product Officer each quarter per the details and timeline below. 

#### FY22-Q2 Product OKR Process

##### OKR Kick-off by the Product Leadership Team

_Please note that the FY22-Q2 process is a "hybrid" model as we continue to leverage Gitlab epics and issues as SSOT for OKR content but shift to scoring OKRs in Ally.io.  We fully transition into Ally.io in FY22-Q3. Thank you for your patience!_

- **5 weeks** prior to the start of the fiscal quarter, the CEO and Chief of Staff initiate the OKR process. The CEO will share an MR in the #e-group Slack channel with the 3 Company Objectives for the quarter, and one to three Key Results for each objective.
- **4 weeks** prior to the start of the new quarter the Chief Product Officer will draft their OKRs in a GitLab issue in the [Product Project](https://gitlab.com/gitlab-com/Product/-/issues) and review the drafted Product OKRs with the CEO.
- **3 weeks** prior to the start of the new quarter, the CEO approves the OKRs and the Chief Product Officer shares the OKRs with the [product leadership](/handbook/product/product-leadership/) team for alignment and to finalize ownership of the KRs to specific product team members. 
- **2 weeks** prior to the start of the new quarter, the EBA to the Chief Product Officer will port the finalized OKRs into [Ally.io](https://app.ally.io/teams/39480/objectives?tab=0&time_period_id=135090) and assign the Objectives and Key Results to the designated owners, as specified by the Chief Product Officer in the finalized Gitlab epics/issues.
     - The EBA will align the Product OKRs to their corresponding parent-level OKRs so everything cascades from the Company Objectives set by the CEO and Chief of Staff.
     - The EBA will link the OKR descriptions in Ally.io to the related GitLab epics/issues for ease of reference to the final SSOT.

##### OKR Collaboration by the rest of Product Team

**Updating the status of Product OKRs**

- Team members who **own** specific OKRs will update the status in the relevant GitLab epics/issues as appropriate every 4 weeks and ping any relevant stakeholders. For example, since FY22-Q2 begins May 1, KR owners will provide status as needs by  June 1 and by July 1. 
     - It is recommended to direct link to the monthly update in the GitLab epic/issue in the Ally.io OKR description to maintain a clear reference to the SSOT. 
- Team members who **own** specific OKRs will update [the score](https://about.gitlab.com/company/okrs/#scoring-okrs) in Ally.io every 4 weeks and tag any relevant stakeholders

**Note:** OKR owners will get reminders via Slack/Ally.io on or near the first of each month to do this. To set up more reminders or opt out of reminders, check out [how to use Ally.io](#how-to-use-allyio)   

#### How to use Ally.io

For an overview of how Product is currently using Ally.io and how to do some basic tasks, check out this [overview video](https://youtu.be/hP9yk_PSj2k). We also have additional training materials provided by Ally.io and Gitlab in [this issue](https://gitlab.com/gitlab-com/ally-okr-rollout/-/issues/9) We recommend you check out everything in this issue but these are some specific links for common topics:

- [How to access Ally.io](https://gitlab.com/gitlab-com/ally-okr-rollout/-/issues/9#how-to-access-ally)
- [How Ally.io and Slack currently work together](https://gitlab.com/gitlab-com/ally-okr-rollout/-/issues/9#slack-integration)
- [How Ally.io and GitLab currently work together](https://gitlab.com/gitlab-com/ally-okr-rollout/-/issues/9#ally-and-gitlab-integration) 
- [How to change the scoring and do check-ins of an OKR in Ally.io](https://gitlab.com/gitlab-com/ally-okr-rollout/-/issues/9#what-are-check-ins)
- [How to update the various details or descriptions of an OKR in Ally.io](https://gitlab.com/gitlab-com/ally-okr-rollout/-/issues/9#how-to-create-okrs)]

#### FY22-Q3 Product OKR Process

In FY22-Q3 we'll have an updated process to fully leverage Ally.io as SSOT for OKRs and sunset usage of GitLab epics/issues. Details forthcoming... 

##### OKR Collaboration by the rest of Product Team

**Updating the status of Product OKRs**

 - TBD

**Contributing to Product OKRs**

The Product team encourages the creation of product group level OKRs, as well as additional Key Results, created and owned by product managers who want to contribute to specific Product Objectives. To do so, please follow the guidance below.  

- TBD

### Product OKRs by Quarter

- [Product OKRs FY22-Q2 (active)](https://gitlab.com/gitlab-com/Product/-/issues/2376)
- [Product OKRs FY22-Q1](https://gitlab.com/gitlab-com/Product/-/issues/1914)
- [Product OKRs FY21-Q4](https://gitlab.com/gitlab-com/Product/-/issues/1566)
- [Product OKRs FY21-Q3](https://gitlab.com/gitlab-com/Product/-/issues/1320)

### How to contribute to this page

We encourage suggestions and improvements to the Product OKR process so please feel free to raise an MR to this page. To keep us all aligned, as the process touches all teams across the company, please assign all MRs for collaboration, review and approval to [Product Operations](https://gitlab.com/fseifoddini) and the [EBA to the Chief Product Officer](https://gitlab.com/kristie.thomas). 

