---
layout: handbook-page-toc
title: Zuora
description: “Discover how GitLab uses Zuora to automate subscription operations”
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Background

[Zuora](/handbook/engineering/development/fulfillment/architecture/#zuora), a leader in the subscription management technology space, provides a platform which allows automation of subscription operations for businesses. We use Zuora's platform at GitLab to integrate with other platforms for billing and CRM, automate subscriptions flows and assist with reporting. The platform is considered the source of truth for many important business objects, like the product catalog, subscriptions, invoices, and more. More information on the Zuora Object Model can be found in [Zuora's Knowledge Center](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/D_Zuora_Business_Objects_Relationship).

Zuora's Portal is an important system in a core group we work with commonly in Fulfillment. [CustomersDot](/handbook/engineering/development/fulfillment/architecture/#customersdot) is an application which provides our customers a portal to purchase and manage GitLab subscriptions, manage payment methods, and view invoices. The application relies heavily on the features provided by Zuora. Please see the system definitions on [the Fulfillment Software Architecture page](/handbook/engineering/development/fulfillment/architecture/#system-definitions) for more information.

As the [CustomersDot](/handbook/engineering/development/fulfillment/architecture/#customersdot) application has evolved over time, Zuora is relied on heavily to provide key data. However, some of these data points are stored in [CustomersDot](/handbook/engineering/development/fulfillment/architecture/#customersdot) and must be kept in sync with Zuora. This syncing has proven to be difficult to get correct for a variety of reasons. You can find many of these issues identified in a [Data Integrity Epic](https://gitlab.com/groups/gitlab-org/-/epics/4307). We feel like getting this sync correct may not be feasible.

# Vision

We feel the prudent decision is to treat Zuora as a [Single Source of Truth](https://about.gitlab.com/handbook/handbook-usage/#single-source-of-truth) and avoid duplicating data from Zuora in other systems, particularly [CustomersDot](/handbook/engineering/development/fulfillment/architecture/#customersdot).

The following is a list of proposed changes to help address our Zuora sync issues:

- Cache the product catalog locally in [CustomersDot](/handbook/engineering/development/fulfillment/architecture/#customersdot). [IronBank](https://github.com/zendesk/iron_bank), a gem used in [CustomersDot](/handbook/engineering/development/fulfillment/architecture/#customersdot) to interface with the Zuora API, has a feature to cache the catalog locally. This has [already been implemented](https://gitlab.com/gitlab-org/customers-gitlab-com/-/merge_requests/1762) but will need some modifications.
  - A custom flag will be needed to indicate if a product should be listed on [CustomersDot](/handbook/engineering/development/fulfillment/architecture/#customersdot) publicly.
  - Automatic cache invalidation when products change in Zuora.
- The `Order` model should not be used for paid subscriptions. Only for trials going forward.
  - All data about paid subscriptions should be stored on the Subscription object in Zuora.
  - Custom fields may be necessary for namespace, CI minutes, license key delivery, and more.
- Remove fields in the [CustomersDot](/handbook/engineering/development/fulfillment/architecture/#customersdot) DB that overlap with Zuora.
  - This is a [list of columns](https://docs.google.com/spreadsheets/d/1TSeaQlMG8zopRqR_0ZcX0IKLN3IpIb3AY-36m6p6pc4/edit#gid=0) from the [LicenseDot](/handbook/engineering/development/fulfillment/architecture/#licensedot) and [CustomersDot](/handbook/engineering/development/fulfillment/architecture/#customersdot) DBs that have some overlap with Zuora.

As we go forward with the changes proposed above, we may not be able to call Zuora the SSoT as there are still some data points that are necessary in both or multiple systems. For example, there is a sync between Salesforce and Zuora and thus duplicated data between them.

There are also some edge cases that need consideration. For instance, some customer data would still need to exist in [CustomersDot](/handbook/engineering/development/fulfillment/architecture/#customersdot) and Zuora, at least until we determine a way to handle first time users who register before purchasing a subscription. We've identified this as a case where we store their information in the [CustomersDot](/handbook/engineering/development/fulfillment/architecture/#customersdot) DB, but not in Zuora because it would bloat the Zuora DB with customers who are simply previewing a purchase. If we decide to handle this differently (e.g. storing them all in Zuora), then it's possible we can remove some duplication here.

The vision is to treat Zuora as the source of record for paid subscriptions and eliminate duplication of Zuora data in other systems.

## Pros
- Eliminate sync or state issues related to the `Order` model
  - Syncing CI minutes to GitLab.com
  - Assigning the paid plan on GitLab.com
  - Delivery of the license keys
- Improved workflows for internal teams
  - Sales reps or Support team can easily check custom fields in Zuora
  - Easier monitoring of sync using custom Zuora fields (e.g. license delivery, namespace plan sync)
- [CustomersDot](/handbook/engineering/development/fulfillment/architecture/#customersdot) codebase will be easier to maintain and reason about
  - Clearer division of responsibility with Subscription information only in Zuora

## Cons
- Single point of failure with reliance on Zuora
  - [CustomersDot](/handbook/engineering/development/fulfillment/architecture/#customersdot) already has a reliance on Zuora currently but this increases this reliance
- More platform dependent for on a particular subscription service
- Introduces more custom fields on Zuora objects like Subscription
  - Must also be added to Salesforce as well
  - Management of migrations will be a manual process with more coordination with other teams
  - Even more important to keep the Zuora sandbox schema and product catalog up-to-date with production
- Slower performance with more reliance on Zuora API
  - This could be minor as there are already quite a few queries made to the Zuora API
  - We could explore increasing our [concurrent request limit](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/Policies/Concurrent_Request_Limits) with Zuora
- More difficulty working in the test suite by increasing dependence on Zuora stubs/mocks or VCR.

## Epic

We have created [an epic](https://gitlab.com/groups/gitlab-org/-/epics/4664) to track the changes as we attempt to transition to using Zuora as the SSoT. You can find issues related to the proposed changes above in this epic.
