---
layout: job_family_page
title: "Technology Partner Manager"
---

## Levels

### Associate Technology Partner Manager

The Associate Technology Partner Manager reports to Manager, Technology Partner Managers.

#### Associate Technology Partner Manager Job Grade

The Associate Technology Partner Manager is a [grade 5](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Associate Technology Partner Manager Responsibilities

1. Execute annual partner plans with technology companies including ISVs and cloud companies.
1. Evaluate effectivity of partner relationships and engagements in delivering positive ROI.
1. Be the central point of contact and advocacy at GitLab, orchestrating the partner engagements across the various departments: product, content, marketing, and sales
1. Ensure successful execution of marketing and sales acceleration programs, with a drive for measurable outcomes and revenue yield
1. Facilitate meaningful relationships between GitLab and our technology partners' teams. This includes partners executive leaders, product managers, engineering, sales and marketing.
1. Identify new opportunities for quality, high ROI engagements with partners

#### Associate Technology Partner Manager Requirements

1. Sales management and/or business development experience in the technology/cloud services industry, preferably in the DevOps space
1. Demonstrated repeated success leading partner/client engagement plans to fruitful executions, consistently exceeding key performance metrics
1. Strong presentation skills and the ability to articulate complex concepts to cross functional audiences
1. Presentation skills with a high degree of comfort speaking with executives, IT Management, and developers
2. Significant experience with executive presence and a proven ability to lead and facilitate executive meetings including architecture whiteboards
1. Excellent time management and written/verbal communication skills
1. Ability to quickly understand technical concepts and explain them to audiences of varying technical expertise
1. [Leadership at GitLab](https://about.gitlab.com/company/team/structure/#management-group)
1. Ability to use GitLab

### Technology Partner Manager (Intermediate)

The Technology Partner Manager (Intermediate) reports to Manager, Technology Partner Managers.

#### Technology Partner Manager (Intermediate) Job Grade

The Technology Partner Manager (Intermediate) is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Technology Partner Manager (Intermediate) Responsibilities

* Extends that of thet Junior Technology Partner Manager responsibilities
* Lead the strategy, sales and integrations for our key partners
* The role will interface significantly with our PM org and be responsible for initial technical scoping and detailed business strategy

#### Technology Partner Manager (Intermediate) Requirements

* Extends that of the Technology Partner Manager (Intermediate) Requirements
* Sales management and/or business development experience in the technology/cloud services industry, preferably in the DevOps space

### Senior Technology Partner Manager

The Senior Technology Partner Manager reports to Manager, Technology Partner Managers.

#### Senior Technology Partner Manager Job Grade

The Senior Technology Partner Manager is a [grade 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Senior Technology Partner Manager Responsibilities

* Extends that of the Technology Partner Manager (Intermediate) responsibilities
1. Own and lead C-Suite relationships
2. Speak regularly in public as the voice for GitLab and the partner
3. SA level expertise with GitLab AND the partners technology
4. Proactively identify, develop and drive longer-term strategies and initiatives that will deliver rapid business growth globally in conjunction with alliance partners.

#### Senior Technology Partner Manager Requirements

* Extends that of the Technology Partner Manager (Intermediate) Requirements
* Sales management and/or business development experience in the technology/cloud services industry, preferably in the DevOps space

### Manager, Technology Partner Managers

The Manager, Technology Partner Managers reports to the VP, Alliances.

#### Manager, Technology Partner Managers Job Grade

The Manager, Technology Partner Managers is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Manager, Technology Partner Managers Responsibilities

* Execute annual partner plans with technology companies including ISVs, cloud, and infrastructure companies.
* Evaluate the effectiveness of partner relationships and engagements in delivering positive ROI and impact to bottom line revenue. 
* Be the central point or an escalation point of contact at GitLab, orchestrating the partner engagements across the * various departments: product, content, marketing, and sales.
* Ensure successful execution of marketing and sales acceleration programs, with a drive for measurable outcomes and revenue yield
* Facilitate meaningful relationships between GitLab and our technology partners' teams. This includes partners, executive leaders, product managers, engineering, sales and marketing.
* Identify and nurture new opportunities for quality, high ROI engagements with partners. 

#### Manager, Technology Partner Managers Requirements

* Business development experience or directly managing partner relationships a GTM strategies in the technology/cloud services industry, preferably in the DevOps space.
* Demonstrated repeated success leading partner/client engagement plans to fruitful executions, consistently exceeding key performance metrics.
* Build out and own executive VP and C-level relationships across various technology partners. 
* Strong presentation skills and the ability to articulate complex concepts to cross functional audiences.
* Presentation skills with a high degree of comfort speaking with executives, IT Management, and developers
* Significant experience with executive presence and a proven ability to lead and facilitate executive meetings including architecture whiteboards
* Excellent time management and written/verbal communication skills
* Works collaboratively between partners and Gitlab sales team to drive white space discussion and opportunity generation.
* Ability to quickly understand technical concepts and explain them to audiences of varying technical expertise
* Leadership at GitLab
* Ability to use GitLab

## Performance Indicators

* $ derived from GitLab + Technology partner joint customers
* Size of Partner Practices - skilled, certified consultants and deliver resources
* [SMAU](/handbook/product/performance-indicators/#stage-monthly-active-users-smau) acceleration by stage or # seats in named enterprise accounts

## Career Ladder

The next step for the Technology Partner Manager Job Family has not yet been defined.

## Hiring Process

Applicants for this position can expect the hiring process to follow the order below. Please keep in mind that applicants can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team/).

* Selected candidates will be invited to schedule a 30min screening call with one of our Global Recruiters
* Next, candidates will be invited to schedule a first interview with the Hiring Manager
* Next, candidates will be invited to interview with 2-5 team members
* There may be a final executive interview

Additional details about our process can be found on our [hiring page](/handbook/hiring/).
